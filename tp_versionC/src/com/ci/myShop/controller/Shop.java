package com.ci.myShop.controller;

import java.util.List;

public class Shop {

	// Attributs
	public static	List<String> storage;
    public static float cash;    
   
    public Shop (List<String> list,float cash) {
    	Shop.storage = list;
    	Shop.cash = 100;
    }
    //Fonctions
	// permet de vendre un item s'il existe
    public static void sell(String Name){
    	if (storage.contains(Name)) {
    	Storage.getItem(Name).setNbrElt(Storage.getItem(Name).getNbrElt()-1);
    	setCash(cash = cash + Storage.getItem(Name).getPrice());
    	}
    } 
    
    public static void setCash(float cash) {
        Shop.cash = cash;
    }
}
