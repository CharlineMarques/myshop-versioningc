package com.ci.myShop;

import java.util.ArrayList;
import java.util.List;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.controller.Storage;
import com.ci.myShop.model.Item;

public class Launch {

	public static void main(String[] args) {
		//cr�er une liste d'item
		List<String> list = new ArrayList<String>();
		
		//Cr�e deux items 
		Item item1 = new Item("livre", 1, 8, 3);
        Item item2 = new Item("stylo", 2, 1, 7);
        
        //les rentre dans le storage
        Storage.addItem(item1);
        Storage.addItem(item2);
        
        //les associe � la liste d'item
        list.addAll(Storage.itemMap.keySet());
                
        // cr�e un shop initialis� avec la list
        Shop shop = new Shop (list,0);
        
        //execute la fonction sell du Shop avec le nom d'un item existant
        Shop.sell("livre");
        
        //affiche le cash du Shop
        System.out.println(Shop.cash);
	   
        /*System.out.println(item1.getNbrElt());
        System.out.println(Storage.getItem("livre"));
        System.out.println(Storage.getItem("stylo"));
        System.out.println(item1.display());
        System.out.println(item2.display());
        System.out.println(Storage.display());
        System.out.println();
        System.out.println(Shop.storage);*/
        
	}

}
